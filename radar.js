/*jslint node: true*/
var radar = require("svg-radar-chart");
var stringify = require("virtual-dom-stringify");

if (!process.argv[2]) {
    console.error("Missing axis parameter");
    process.exit();
} else if (!process.argv[3]) {
    console.error("Missing values parameter");
    process.exit();
}


var chart = radar(
    JSON.parse(process.argv[2]),
    [
        JSON.parse(process.argv[3])
    ]
);

process.stdout.write(stringify(chart));
