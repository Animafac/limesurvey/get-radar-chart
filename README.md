# get-radar-chart

This tool takes data from a GET parameter and returns a radar chart.

## Setup

You need to install dependencies with [Composer](https://getcomposer.org/) and [Yarn](https://yarnpkg.com/):

```bash
composer install
yarn install
```

## Usage

You need to call `index.php` with a GET request like this: `index.php?variables[foo]=12&variables[bar]=100&variables[baz]=50`

All variables must have a value between 0 and 100.

### CLI

You can also call this tool from the commandline with two JSON arguments containing the labels and the values:

```bash
node radar.js '{ "foo": "Foo", "bar": "Bar", "baz": "Baz" }' '{ "foo": 12, "bar": 100, "baz": 50 }'
```
