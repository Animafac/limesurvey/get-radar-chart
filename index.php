<?php
use Symfony\Component\Process\ProcessBuilder;

require __DIR__.'/vendor/autoload.php';

$axis = [];
$values = [];
foreach ($_GET['variables'] as $name => $value) {
    $axis[] = $name;
    $values[] = $value/100;
}

$builder = ProcessBuilder::create(
    [
        'node', __DIR__.'/radar.js',
        json_encode($axis, JSON_FORCE_OBJECT),
        json_encode($values, JSON_FORCE_OBJECT)
    ]
);
$process = $builder->getProcess();
$process->run();

header('Content-Type: image/svg+xml');

echo '<?xml version="1.0" encoding="utf-8"?>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
    <title>Radar chart</title>
    <style>
        axis {
            stroke: #555;
            stroke-width: .2;
        }
        .scale {
            fill: #f0f0f0;
            stroke: #999;
            stroke-width: .2;
        }
        .shape {
            fill-opacity: .3;
            stroke-width: .5;
            fill: #00a0b0; stroke: #00a0b0;
        }
        .shape:hover {
            fill-opacity: .6;
        }
	</style>'.
    $process->getOutput().
    '</svg>';
